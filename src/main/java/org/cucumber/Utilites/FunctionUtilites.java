package org.cucumber.Utilites;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.cucumber.JunitRunner.Runner;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Sleeper;

import com.sun.media.sound.InvalidFormatException;

public class FunctionUtilites extends Runner
{
/*---------------------------------------------------------------
//**Method Name : ButtonClick
//**This Method is used for Clicking The Button if it is Displayed
//** Author : Koushick.s
 -----------------------------------------------------------------*/
   public void ButtonClick(By by) throws InterruptedException
   {
	  Thread.sleep(3000); 
	  WebElement button = Runner.driver.findElement(by);
	  if(button.isDisplayed())
	  {	  
	  button.click();
	  }
	  else
	  {
		  System.out.println("Can't Find the Button and Click");
	  }	  
   }
 /*---------------------------------------------------------------
 //**Method Name : SendingKeys
 //**This Method is used for sending s message in the given field if it is Displayed
 //** Author : Koushick.s
  -----------------------------------------------------------------*/
   public void SendingKeys(By by,String message) throws InterruptedException
   {
	   
	   Thread.sleep(3000);
	   WebElement Sendkeys = Runner.driver.findElement(by);
	   if(Sendkeys.isDisplayed())
	   {
		   Thread.sleep(3000);
		   Sendkeys.sendKeys(message);
	   }   
	   else {
		System.out.println("Error While Passing Message");
	}
   }
   
   public void SendingKeysSearch(By by,String message) throws InterruptedException, AWTException
   {
	   
	   Thread.sleep(3000);
	   WebElement Sendkeys = Runner.driver.findElement(by);
	   if(Sendkeys.isDisplayed())
	   {
		   Thread.sleep(3000);
		   Robot rb = new Robot();
		   rb.keyPress(KeyEvent.VK_CONTROL);
		   rb.keyPress(KeyEvent.VK_A);
		   rb.keyRelease(KeyEvent.VK_CONTROL);
		   rb.keyRelease(KeyEvent.VK_A);
		   Sendkeys.clear();
		   Sendkeys.sendKeys(message,Keys.ENTER);
	   }   
	   else {
		System.out.println("Error While Passing Message");
	}
   }
   
   /*---------------------------------------------------------------
   //**Method Name : Actionclick
   //**This Method is used for move to a paticular element and click
   //** Author : Koushick.s
    -----------------------------------------------------------------*/
   public void Actionclick(By by) throws InterruptedException
   {
	   Thread.sleep(3000);
	   WebElement Actionclick = Runner.driver.findElement(by);
	   if(Actionclick.isDisplayed())
	   { 
	   Actions act = new Actions(Runner.driver);
	   act.moveToElement(Actionclick).click(Actionclick).build().perform();
	   }
	   else
	   {
		   System.out.println("Can't find and click on the button ");
	   }   
   }
   
   /*---------------------------------------------------------------
   //**Method Name : Screenshot
   //**This Method is used take screenshot and Attach in the Document.
   //** Author : Koushick.s
    -----------------------------------------------------------------*/ 
   @SuppressWarnings("static-access")
  public void Screenshot(String filename) throws org.apache.poi.openxml4j.exceptions.InvalidFormatException, IOException
   {
	   DocumentUtilites doc = new DocumentUtilites();
	   doc.capture(driver, filename);
   }
   
   
   

}
