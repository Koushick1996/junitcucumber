package org.cucumber.Utilites;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.sun.prism.Image;
import com.sun.scenario.effect.ImageData;

import cucumber.api.java.it.Data;

public class DocumentUtilites extends FunctionUtilites
{
	public static XWPFParagraph para;
	public static XWPFRun run;
	static int i = 0;
	static int icapture =1;
	static XWPFDocument doc;
	static FileOutputStream out;
	static FileInputStream File_new_PIC;
	static File new_file;
	static String rootpath = "C:\\Users\\Hp\\workspace\\Cucumber_Junit\\src\\test\\resources\\Results\\";
	static String rootpath_Folder = "C:\\Users\\Hp\\workspace\\Cucumber_Junit\\src\\test\\resources\\Results\\ScreenShots\\";
	static String docname;
	
	public static void capture(WebDriver driver,String filename) throws InvalidFormatException, IOException {
		 try {
			 docname=filename;
			 DocumentUtilites document = new DocumentUtilites();
			 if(icapture==1)
			 {
				document.createdocument(docname); 
			 }
			   doc = new XWPFDocument(new FileInputStream(rootpath+docname+".docx")); 
		    	para = doc.getLastParagraph();
		    	run = para.createRun();
	    // Get the screenshot as an image File
	      File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);	   
	      // Specify the destination where the image will be saved
	      File dest = new File(rootpath_Folder+ docname +icapture + ".png");
	      // Copy the screenshot to destination
	      FileUtils.copyFile(src, dest);
	      i++;
	      File_new_PIC = new FileInputStream(dest);
	      run.addBreak();
	      run.addPicture(File_new_PIC,  XWPFDocument.PICTURE_TYPE_PNG,  docname + i + ".png", Units.toEMU(500), Units.toEMU(200));
	      run.addBreak();
	      System.out.println("Taking Screenshot..");
	      out = new FileOutputStream(rootpath+docname+".docx");
	      doc.write(out);
	      File_new_PIC.close();
		  out.close();
		  doc.close();
	    	  
	
	    } catch (IOException ex) {
	      System.out.println(ex.getMessage());
	    }catch (WebDriverException e) {
			System.out.println("The Browser May be closed");
		}
		 finally{
			 icapture++;
		 }
	       
		 
	  }
	
	public void createdocument(String filename)
	{
		try
		{
		   docname = filename;
		   LocalDateTime now = LocalDateTime.now();
		    doc = new XWPFDocument();
			para = doc.createParagraph();
			para.setAlignment(ParagraphAlignment.CENTER);
			run = para.createRun();
			run.setBold(true);
			new_file = new File(rootpath+filename+".docx");	
			run.addBreak();
			run.addBreak();
			run.setBold(true);
			run.setText("Executing Testcase For : " + filename);
			run.addBreak();
			run.setText("Executed on : " + now);
			run.addBreak();
			FileOutputStream createfile = new FileOutputStream(new_file);
			doc.write(createfile);
			createfile.close();
			doc.close();
			System.out.println("Document is Created");
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	}
}
