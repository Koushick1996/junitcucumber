package org.cucumber.Utilites;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import com.sun.org.apache.xpath.internal.axes.HasPositionalPredChecker;

public class ExcelWriter extends FunctionUtilites
{
     FileInputStream fis;
	 String rowvalue;
     String Colvalue;
     String returndata;
		   
	public String Read_Excel(String data,int i) throws IOException
	{
        try {
       	 String filename = "C:\\Users\\Hp\\workspace\\testngcucumberframework\\testngcucumberframework\\src\\test\\resources\\Datatable\\MasterTestdata.xlsx";
            fis = new FileInputStream(filename);
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator rowIter = sheet.rowIterator(); 
 
            while(rowIter.hasNext()){
                XSSFRow myRow = (XSSFRow) rowIter.next();
                Iterator cellIter = myRow.cellIterator();
                Vector<String> cellStoreVector=new Vector<String>();
                while(cellIter.hasNext()){
                	XSSFCell myCell = (XSSFCell) cellIter.next();
                	XSSFCell mycol = myRow.getCell(0);
 	
                    switch (myCell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						rowvalue =""+ myCell.getNumericCellValue();
						break;
					case Cell.CELL_TYPE_STRING:
						rowvalue = myCell.getStringCellValue();
                        break;
					case Cell.CELL_TYPE_BLANK:
						rowvalue = "[BLANK]";
						break;
					case Cell.CELL_TYPE_ERROR:
						rowvalue = myCell.getErrorCellString();
						break;
					}
                    
                    switch (mycol.getCellType()) {
         					case Cell.CELL_TYPE_NUMERIC:
         						Colvalue = ""+ mycol.getNumericCellValue();
         						break;
         					case Cell.CELL_TYPE_STRING:
         						Colvalue = mycol.getStringCellValue();
                                 break;
         					case Cell.CELL_TYPE_BLANK:
         						Colvalue = "[BLANK]";
         						break;
         					case Cell.CELL_TYPE_ERROR:
         						Colvalue = mycol.getErrorCellString();
         						break;
         					}
                	cellStoreVector.addElement(rowvalue);
                }
                    if(data.matches(Colvalue))
                    {
                    	System.out.println(Colvalue);
                    	returndata = cellStoreVector.get(i);
                    	System.out.println(returndata);
                    }
                    else
                    {
                    	System.out.println("No Match Found.");
                    }    
            }
        } catch (IOException e) {
 
            e.printStackTrace();
 
        } finally {
 
            if (fis != null) {
 
                fis.close();
 
            }
 
        }
		return returndata;
 
      //showExelData(sheetData);
 
    }
}
