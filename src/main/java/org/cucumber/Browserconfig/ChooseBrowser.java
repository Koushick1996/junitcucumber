package org.cucumber.Browserconfig;


import org.cucumber.JunitRunner.Runner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ChooseBrowser 
{
	
	public WebDriver Selectbrowser(String browser,String url) throws Exception
	{
		if(browser.equalsIgnoreCase("Firefox"))
		 {
			 System.setProperty("webdriver.gecko.driver",Runner.prop.getProperty("firefox"));
             Runner.driver = new FirefoxDriver();
		 }
		 else if(browser.equalsIgnoreCase("chrome"))
		 {
			 System.setProperty("webdriver.chrome.driver",Runner.prop.getProperty("chrome"));
			 ChromeOptions opt = new ChromeOptions();
			//opt.addArguments("-incognito");
			 opt.addArguments("-disable-infobars");
			 Runner.driver = new ChromeDriver(opt);
		 }	 
		 else
		 {
			 throw new Exception("Browser is Incorrect!");
		 } 
		 
		Runner.driver.get(url);
		Runner.driver.manage().window().maximize();
		 return Runner.driver;	
	}

}
