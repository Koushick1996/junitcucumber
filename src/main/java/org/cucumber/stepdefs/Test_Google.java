package org.cucumber.stepdefs;

import org.cucumber.Browserconfig.ChooseBrowser;
import org.cucumber.JunitRunner.Runner;
import org.cucumber.Utilites.FunctionUtilites;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Test_Google extends FunctionUtilites{
	
	@Given("^Enter The user \"([^\"]*)\" in The stackoverflow$")
	public void enter_The_user_in_The_stackoverflow(String arg1) throws Throwable {
		ChooseBrowser browser = new ChooseBrowser();
		Runner.driver = browser.Selectbrowser(Runner.prop.getProperty("Browsertype_1"), Runner.prop.getProperty("url_2"));
		System.out.println(arg1);
		Screenshot("GoogleHomepage");
	}

	@And("^I Enter The \"([^\"]*)\" in the password field and clickon Login$")
	public void i_Enter_The_in_the_password_field_and_clickon_Login(String arg1) throws Throwable {
	   Screenshot("GoogleHomepage");
	   WebElement ele = Runner.driver.findElement(By.name("q"));
	   ele.sendKeys(arg1,Keys.ENTER);
	   Screenshot("GoogleHomepage");
	}

	@Then("^I verify the \"([^\"]*)\" of The page$")
	public void i_verify_the_of_The_page(String arg1) throws Throwable {
		System.out.println("Entering The Search Text : " + arg1);
		Screenshot("GoogleHomepage");
	}

}
