package org.cucumber.stepdefs;

import org.openqa.selenium.By;

public class Stackoverflowobj 
{
	
	public static final By LOGIN = By.linkText("Log in");
	public static final By EMAIL = By.id("email");
	public static final By PASSWORD = By.id("password");
	public static final By LOGIN_BUTTON = By.xpath("//button[@id='submit-button']");
	public static final By CHECK_STATUS = By.xpath("//div[@class='grid']/h1");
	public static final By HOME_BUTTON = By.xpath("//ol[@class='-secondary js-secondary-topbar-links drop-icons-responsively user-logged-in the-js-is-handling-responsiveness']/li[2]");
	public static final By SEARCH_FIELD = By.name("q");

}
