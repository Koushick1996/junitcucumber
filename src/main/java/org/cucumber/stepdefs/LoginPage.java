package org.cucumber.stepdefs;
 
import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Savepoint;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import org.cucumber.Browserconfig.ChooseBrowser;
import org.cucumber.JunitRunner.Runner;
import org.cucumber.Objects.Stackoverflowobj;
import org.cucumber.Utilites.DocumentUtilites;
import org.cucumber.Utilites.ExcelWriter;
import org.cucumber.Utilites.FunctionUtilites;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Sleeper;




import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
 

public class LoginPage extends ExcelWriter
{	
	   
	ExcelWriter excelhashmapvalues = new ExcelWriter();
	   
	@Given("^Open Application and Enter url$")
	public void open_Application_and_Enter_url() throws Throwable 
	{
		ChooseBrowser browser = new ChooseBrowser();
		Runner.driver = browser.Selectbrowser(Runner.prop.getProperty("Browsertype_1"), Runner.prop.getProperty("url_1"));
		 
	}
	@When("^enter username$")
	public void enter_username() throws Throwable {
		ButtonClick(Stackoverflowobj.LOGIN);
		Screenshot("Stackoverflow");
		Sleep(3000);
		SendingKeys(Stackoverflowobj.EMAIL, Runner.prop.getProperty("username"));
		Screenshot("Stackoverflow");
	}

	public void Sleep(int i) throws InterruptedException {
		Thread.sleep(i);
	}

	@When("^enter password$")
	public void enter_password() throws Throwable {
        SendingKeys(Stackoverflowobj.PASSWORD, Runner.prop.getProperty("password"));
        Screenshot("Stackoverflow");       
        ButtonClick(Stackoverflowobj.LOGIN_BUTTON);
		Sleep(5000);
	}
	
	@SuppressWarnings("static-access")
	@Then("^verify Msg$")
	public void verify_Msg() throws Throwable {
	   boolean result =  Runner.driver.findElement(Stackoverflowobj.CHECK_STATUS).getText().contains("Top Questions");
	   junit.framework.Assert.assertTrue(result);
	   Screenshot("Stackoverflow");
	   ButtonClick(Stackoverflowobj.HOME_BUTTON);
	   Screenshot("Stackoverflow");
	   SendingKeysSearch(Stackoverflowobj.SEARCH_FIELD, excelhashmapvalues.Read_Excel("SearchKey", 1));
	   Sleep(2000);
	   Screenshot("Stackoverflow");
	}

}