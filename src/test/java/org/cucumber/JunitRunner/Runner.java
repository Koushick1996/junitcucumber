package org.cucumber.JunitRunner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import gherkin.formatter.model.Feature;

import org.cucumber.Browserconfig.ChooseBrowser;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\resources\\Features\\Login.feature",
glue={"org.cucumber.stepdefs"},dryRun=false,
tags="@All_1")
public class Runner extends ChooseBrowser {
	
	
	public static WebDriver driver;
	public static Properties prop = new Properties();
	private final static String propertyFilePath= "C:\\Users\\Hp\\workspace\\Cucumber_Junit\\config.properties";
	static BufferedReader reader;
	static BufferedReader readernew;
	static BufferedReader readernew1;
	public static String testCaseName = "LoginPageStackover";
	
	@BeforeClass
	public static void setup() throws IOException
	{
		System.out.println("*****---Stack overflow Execution is Started---*******");
		
		System.out.println("***Reading Property Files***");
		
		reader = new BufferedReader(new FileReader(propertyFilePath));
		prop.load(reader);
		String PropertyFilepathnew = prop.getProperty("role");
		readernew = new BufferedReader(new FileReader(PropertyFilepathnew));
		prop.load(readernew);
		String PropertyFilepathnew1 = prop.getProperty("directory");
		readernew1 = new BufferedReader(new FileReader(PropertyFilepathnew1));
		prop.load(readernew1);
		System.out.println(prop);
		
		System.out.println("Executing the Feature files");
	}
	@AfterClass
	public static void Teardown()
	{
		System.out.println("Driver has been closed");
		driver.quit();
	}

}
